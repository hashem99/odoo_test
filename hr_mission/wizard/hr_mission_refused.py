# -*- coding: utf-8 -*-
""" init object hr.mission.refused """
import logging

from odoo import _, api, fields, models
from odoo.exceptions import UserError

LOGGER = logging.getLogger(__name__)


class HrMissionRefused(models.TransientModel):
    """ init object hr.mission.refused """
    _name = 'hr.mission.refused'

    @api.multi
    def action_refuse(self):
        """
        Action Refuse Selected Mission with reason.
        """
        self.ensure_one()
        active_ids = self.env.context.get('active_ids')
        active_model = self.env.context.get('active_model')
        if active_model != "hr.mission":
            raise UserError(_("Error! The Refuse Not Apply "
                              "to this model %s") % active_model)
        active_records = self.env[active_model].browse(active_ids)
        if not active_records:
            raise UserError(_("Error! Actually no mission selected."))
        active_records.action_refuse_with_reason()

    reason_refused = fields.Text(required=True)
