# -*- coding: utf-8 -*-
"""Controllers for Page Mission"""

import logging

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo import http
from odoo.http import request

LOGGER = logging.getLogger(__name__)


# pylint: disable=no-self-use
class Dashboard(http.Controller):
    """Controller for  Mission page """

    # pylint: disable=unused-argument, too-many-locals
    @http.route(['/my/mission'], type='http', auth="user", website=True)
    def employee_dashboard(self, **kw):
        """Render the Mission page."""
        user = request.env.user
        employee_ids = (user.employee_ids[0] if user.employee_ids else None)
        missions = request.env['hr.mission'].search(
            [('employee_id', 'in', employee_ids.ids)],
        )

        values = {
            'user': user,
            'missions': missions,
        }
        return request.render('hr_mission.mission_template', values)
