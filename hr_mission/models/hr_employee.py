# -*- coding: utf-8 -*-
""" init object hr.employee"""
import logging

from odoo import api, fields, models
from odoo.osv import expression

LOGGER = logging.getLogger(__name__)


class HrEmployee(models.Model):
    """ init object hr.employee """
    _inherit = 'hr.employee'

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        """
        Override Name Search to search by name or employee code.
        """
        if name and operator in ('ilike', 'like', '=', '=like', '=ilike'):
            domain = expression.AND([
                args or [],
                ['|', ('name', operator, name),
                 ('employee_code', operator, name)]
            ])
            return self.search(domain, limit=limit).name_get()
        return super(HrEmployee, self).name_search(name, args, operator, limit)

    employee_code = fields.Char()
