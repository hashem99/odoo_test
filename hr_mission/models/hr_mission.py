# -*- coding: utf-8 -*-
""" init object hr.mission"""

import logging

from datetime import datetime
from odoo import SUPERUSER_ID
from odoo import _, api, fields, models
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DTF

LOGGER = logging.getLogger(__name__)


class HrMission(models.Model):
    """ init object update hr.mission"""
    _name = 'hr.mission'
    _inherit = ['mail.thread']

    @api.multi
    def unlink(self):
        """
        Override unlink to raise in not draft.
        """
        for record in self:
            if record.state != 'draft':
                raise UserError(_('You Can Delete Mission Only '
                                  'if Status is Draft'))
        return super(HrMission, self).unlink()

    def _employee_get(self):
        """
        Get Default employee related to current user.
        :return: employee <hr.employee>
        """
        employee_id = self.env['hr.employee'].search(
            [('user_id', '=', self.env.user.id)], limit=1, )
        if employee_id:
            return employee_id

    @api.constrains('start_date', 'end_date')
    def _constraints_dates(self):
        """
        Function to constraints dates inserted.
        """
        if self.start_date and self.end_date \
                and self.start_date >= self.end_date:
            raise UserError(_("Start Date Must be before End Date."))

    @api.multi
    def action_confirm(self):
        """
        Action Confirm [Send to DM to approve].
        """
        for rec in self:
            if rec.state == 'draft':
                rec.state = 'confirm'

    @api.multi
    def action_dm_approve(self):
        """
        Action DM Approve [Send to HRM to approve].
        """
        for rec in self:
            if rec.state == 'confirm':
                if (rec.employee_id.parent_id \
                    and rec.employee_id.parent_id.user_id \
                    and rec.employee_id.parent_id.user_id == self.env.user) \
                        or (self.env.user.has_group("hr.group_hr_user")):
                    rec.state = 'first_approve'
                else:
                    raise UserError(_("Only DM Or HRM Can "
                                      "Approve Mission Request."))

    def action_refuse_with_reason(self, reason_refused=None):
        """
        Action Refuse with reason with check privileges.
        :param reason_refused: required <string>
        """
        for rec in self:
            if (rec.employee_id.parent_id \
                and rec.employee_id.parent_id.user_id \
                and rec.employee_id.parent_id.user_id == self.env.user) \
                    or (self.env.user.has_group("hr.group_hr_user")):
                rec.write({'refused_by_id': self.env.user.id,
                           'reason_refused': reason_refused,
                           'state': 'refused', })
            else:
                raise UserError(_("Only DM Or HRM Can "
                                  "Refuse Mission Request."))

    @api.multi
    def action_hrm_approve(self):
        """
        Action HRM Approve [this final approve].
        """
        for rec in self:
            if rec.state == 'first_approve':
                if self.env.user.has_group("hr.group_hr_user"):
                    rec.state = 'approved'
                else:
                    raise UserError(_("Only HRM Can Approve Mission Request."))

    def _domain_employee(self):
        """
        Function set Domain Employee As privileges.
        :return: list tuple [domain]
        """
        employee_id = self.env['hr.employee'].search(
            [('user_id', '=', self.env.user.id)], limit=1, )
        if self.env.user.has_group("hr.group_hr_user"):
            if employee_id:
                return ['|', ('gender', '=', 'male'),
                        ('id', '=', employee_id.id)]
            else:
                return [('gender', '=', 'male')]
        elif self.env.user.id == SUPERUSER_ID:
            return []
        else:
            if employee_id:
                return [('id', '=', employee_id.id)]
            else:
                return [('id', '=', 0)]

    @api.multi
    @api.depends("start_date", "end_date")
    def _compute_period(self):
        """
        Compute Period.
        """
        for rec in self:
            if rec.start_date and rec.end_date:
                start_datetime = datetime.strptime(rec.start_date, DTF)
                end_datetime = datetime.strptime(rec.end_date, DTF)
                if end_datetime > start_datetime:
                    diff = end_datetime - start_datetime
                    rec.period = diff.days * 24 + diff.seconds / 3600

    name = fields.Char(string="Description", required=True,
                       track_visibility='onchange')
    employee_id = fields.Many2one(comodel_name="hr.employee", string="Employee",
                                  default=_employee_get, required=True,
                                  domain=_domain_employee,
                                  track_visibility='onchange')
    department_id = fields.Many2one(comodel_name='hr.department',
                                    string="Department", readonly=True,
                                    related="employee_id.department_id",
                                    track_visibility='onchange', store=True)
    job_id = fields.Many2one(comodel_name='hr.job', string='Job Position',
                             readonly=True, track_visibility='onchange',
                             related="employee_id.job_id", store=True)
    start_date = fields.Datetime(required=True, track_visibility='onchange')
    end_date = fields.Datetime(required=True, track_visibility='onchange')
    period = fields.Float(string="Period By Hours", compute=_compute_period,
                          store=True)
    mission_type = fields.Selection(
        selection=[('personal', 'Personal'),
                   ('work_related', 'Work Related')],
        required=True,
        track_visibility='onchange',
    )
    state = fields.Selection(
        string="Status",
        selection=[('draft', 'Draft'),
                   ('confirm', 'Waiting DM Approval'),
                   ('first_approve',
                    'Waiting HRM Approval'),
                   ('approved', 'Approved'),
                   ('refused', 'Refused'), ],
        default="draft",
        track_visibility='onchange',
    )
    refused_by_id = fields.Many2one(comodel_name="res.users",
                                    string="Refused By",
                                    track_visibility='onchange')
    reason_refused = fields.Text(track_visibility='onchange')
