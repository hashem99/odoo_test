# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, manifest-version-format
# pylint: disable=manifest-required-author
{
    'name': 'HR Mission',
    'summary': 'Handel request mission for employees',
    'author': "Hashem Aly",
    'website': "mailto:smart.hashem@gmail.com",
    'category': 'hr',
    'version': '11.0.1.0.0',
    'depends': [
        'website',
        'portal',
        'hr',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/rules.xml',
        # Important Note: this wizard must be load first.
        'wizard/hr_mission_refused.xml',
        'views/hr_employee.xml',
        'views/hr_mission.xml',
        'templates/mission.xml',
        'templates/home.xml',
        'data/website_menu.xml',
        # Qweb reports
        'report/hr_mission.xml',
    ],
    'demo': [
        'demo/res_users.xml',
    ],
}
