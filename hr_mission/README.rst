.. class:: text-left

Hr Mission
==========

Handel request mission for employees
------------------------------------


Features
--------

#. employee request mission
#. cycle approval for the missions


Credits
-------

.. |copy| unicode:: U+000A9 .. COPYRIGHT SIGN
.. |tm| unicode:: U+2122 .. TRADEMARK SIGN

- `Hashem Aly <smart.hashem@gmail.com>`_ |copy|_ |tm| 2018
