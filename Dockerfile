FROM odoo:11.0
MAINTAINER Hashem Aly <smart.hashem@gmail.com>

# dev 
USER root


# Create .ssh directory
RUN chown odoo: /mnt/extra-addons


RUN apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y \
        ipython3 \
        pylint3 \
        pyflakes \
        python3-flake8 \
        python3-ipdb \
    # install it with its recommended packages
    && apt-get install -y \
        python3-coverage \
    && pip3 install --pre pylint_odoo \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

USER odoo

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8

USER root
COPY odoo.conf /etc/odoo/

# Install watchdog and XLWT
RUN apt-get update \
    && apt-get install -y --no-install-recommends --no-install-suggests \
        python3-watchdog \
        python3-xlrd \
    && pip3 install xlwt \
    && pip3 install simplejson \
    && rm -rf /var/lib/apt/lists/*

USER odoo

COPY . /mnt/extra-addons